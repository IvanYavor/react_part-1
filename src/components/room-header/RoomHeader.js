import React from "react";
import "./RoomHeader.css";

function RoomHeader(props) {
  return (
    <div className="RoomHeader">
      <h1>{props.chatroomName}</h1>
      <p>{props.numberOfParticipants} participants</p>
      <p>{props.numberOfMessages} messages</p>
      <p className="last-message-at">last message at {props.lastMessageAt}</p>
    </div>
  );
}

export default RoomHeader;

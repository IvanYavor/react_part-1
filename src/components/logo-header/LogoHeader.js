import React from "react";
import logo from "./logo.png";
import "./LogoHeader.css";

function LogoHeader() {
  return (
    <div className="LogoHeader">
      <img src={logo} className="App-logo" alt="Logo" />
    </div>
  );
}

export default LogoHeader;

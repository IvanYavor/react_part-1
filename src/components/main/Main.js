import React from "react";
import "./Main.css";
import ChatField from "../chat-field/ChatField";

function Main(props) {
  return (
    <section className="Main">
      <ChatField
        messages={props.messages}
        me={props.me}
        onSendNewMessage={props.onSendNewMessage}
      />
    </section>
  );
}

export default Main;

import React from "react";
import "./Header.css";
import LogoHeader from "../logo-header/LogoHeader";
import RoomHeader from "../room-header/RoomHeader";

function Header(props) {
  return (
    <header>
      <LogoHeader />
      <RoomHeader
        chatroomName={props.chatroomName}
        numberOfParticipants={props.numberOfParticipants}
        participants
        numberOfMessages={props.numberOfMessages}
        lastMessageAt={props.lastMessageAt}
      />
    </header>
  );
}

export default Header;

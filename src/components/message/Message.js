import React from "react";
import "./Message.css";

function Message(props) {
  return (
    <div
      className={
        props.me.userId === props.message.userId
          ? "Message my-message"
          : "Message"
      }
    >
      <div className="message-sender-icon">
        <img src={props.message.avatar} alt="avatar" />
      </div>
      <div className="message-bubble">
        <div className="message-sender-name">{props.message.user}</div>
        <div className="message-sender-content">{props.message.text}</div>
      </div>
    </div>
  );
}

export default Message;

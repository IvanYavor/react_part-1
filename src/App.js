import React from "react";
import messages from "./data";
import "./App.css";
import Header from "./components/header/Header";
import Main from "./components/main/Main";
import _uniqueId from "lodash/uniqueId";
import moment from "moment";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chatroomName: "My Chat",
      numberOfParticipants: 3,
      lastMessageAt: "14.28",
      me: {
        userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce5",
        user: "Ivan",
      },
      messages: messages.messages,
      numberOfMessages: 0,
    };
  }

  onSendNewMessage = async (message) => {
    const currentMessages = this.state.messages;
    const newMessage = {
      text: message,
      user: "Ivan",
      userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce5",
      id: _uniqueId(""),
      createdAt: Date.now(),
      editedAt: "",
      avatar:
        "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
    };
    currentMessages.push(newMessage);
    const numberOfMessages = this.state.messages.length;
    const newStateFragment = {
      numberOfMessages: numberOfMessages,
      messages: currentMessages,
    };
    this.setState({ ...this.state, ...newStateFragment });
  };

  render() {
    const numberOfMessages = this.state.messages.length;
    const lastMessageAt = moment(
      [...this.state.messages].pop().createdAt
    ).format("HH:mm");
    return (
      <div className="App">
        <Header
          chatroomName={this.state.chatroomName}
          numberOfMessages={numberOfMessages}
          numberOfParticipants={this.state.numberOfParticipants}
          lastMessageAt={lastMessageAt}
          me={this.state.me}
        ></Header>

        <Main
          onSendNewMessage={this.onSendNewMessage}
          messages={this.state.messages}
          me={this.state.me}
        />
      </div>
    );
  }
}

export default App;
